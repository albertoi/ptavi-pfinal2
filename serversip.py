#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time

try:
    PORT = int(sys.argv[1])
except ValueError:
    print("Usage: python3 ServerRTP.py <port>")


class SIPRequest:
    def __init__(self, data):
        self.data = data.decode('utf-8')

    def parse(self):
        self._parse_command(self.data)

    def _get_address(self, uri):
        address = uri.split(':')[1]
        schema = uri.split(':')[0]
        return address, schema

    def _parse_command(self, line):
        metodo = line.split()
        self.command = metodo[0]
        self.uri = metodo[1]
        self.address, schema = self._get_address(self.uri)
        if self.command == 'REGISTER':
            if schema == 'sip':
                self.result = '200 OK'
            else:
                self.result = '416 Unsupported URI Scheme'
        if self.command == 'INVITE':
            self.result = '302 Moved Temporarily'
            # Para ACK no vamos a hacer nada
        if self.command == 'ACK':
            pass


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    Dicc = {}

    def json2registered(self):
        try:
            with open('registered.json', 'r') as file:
                self.Dicc = json.load(file)
        except FileNotFoundError:
            self.Dicc = {}

    def registered2json(self, sip, real):
        # Actualizamos diccionario
        self.Dicc[sip] = real
        with open("registered.json", "w") as file:
            json.dump(self.Dicc, file, indent=3)  # con indentación de 3 esp

    def search(self, direccion):
        return self.Dicc[direccion]

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        print(TIME + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1])
              + ":" + data.decode('utf-8'))

        sip_request = SIPRequest(data)
        sip_request.parse()
        dirservicio = sip_request.uri
        part1 = dirservicio.split('@')[0]
        # la cabecera contact con su dir real
        contact = part1 + "@" + self.client_address[0] + ":" + str(self.client_address[1])
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.registered2json(dirservicio, contact)
            sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)
            print(TIME + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1]) + ":" + "SIP/2.0 " +
                  sip_request.result + "\r\n\r\n")
        elif (sip_request.command == "INVITE") and (sip_request.result == "302 Moved Temporarily"):
            search1 = self.search(data.decode('utf-8').split()[1])
            if search1 == "Not found":
                notfound = "SIP/2.0 404 Not found\r\n\r\n"
                sock.sendto(notfound.encode(), self.client_address)
                print(TIME + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1]) + ":" + notfound)
            else:
                redirect = "SIP/2.0 " + sip_request.result + "\r\nContact: " + self.Dicc[dirservicio] + "\r\n\r\n"
                sock.sendto(redirect.encode(), self.client_address)
                print(TIME + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1]) + ":" + redirect)

        elif sip_request.command == "ACK":
            print("No enviamos respuesta al ACk")
        else:
            print("Error: El método no es válido")


def main():
    try:
        serv = socketserver.UDPServer(('127.0.0.1', PORT), SIPRegisterHandler)
        print("Starting...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
