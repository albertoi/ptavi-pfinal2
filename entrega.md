# Proyecto final (segunda versión, junio 2023)

## Parte básica
El objetivo que hemos tenido en este proyecto es la implementación de un servicio de emisión de audio a un servicio receptor.
Esta práctica se base en realizar 3 programas: serversip, serverrtp y client. Los programas los he ido ejecutando en ese orden con sus parametros:

python3 serversip.py 6001

python3 serverrtp.py 127.0.0.1:6001 servidor1

python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3


En esta práctica es importante que el cliente es el emisor de los paquetes RTP y el serverrtp el que los recibe. Así, con ayuda de los ficheros send_rtp.py y recv_rtp.py proporcionados por el profesor he podido realizar esa parte del codigo.
He seguido la estructura de los "Historicos" proporcionados por el enunciado, de modo que se entiendan todas las acciones que se estan realizando con su respectivo tiempo.
Después, he realizado las capturas correspondientes, las cuales han salido correctamente.
Comentario:
Los ACKS en la captrua Wireshark vienen, lo unico es que vienen en el contenido y no como status.


Al principio me salía un malformed en el INVITE(En el wireshrak), y lo resolvi añadiendo una linea en blanco en el invite antes del cuerpo. Despues tuve que cambiar algunas cosas en el RTP, y si es verdad que me sale un pequeño error de algo de bps que antes no me salia... De todas formas se envia todo y el proceso sigue con normalidad. Es decir, ese error de bps me ha salido al añadir la linea en blanco en el invite, pero pienso que no es importante.
## Parte adicional
En cuanto a la parte adicional, estos son las mejoras que he ido introduciendo(SIMPLES):
* Gestión de errores
Aunque no he metido todas las gestiones posibles, si que hay unos pocos que he metido.
* Cabecera de tamaño
La cabecera de tamaño del cuerpo la he incluido, podemos observalos en los invite del cliente.

No he añadido ninguna parte adicional más.

