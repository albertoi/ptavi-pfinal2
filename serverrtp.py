#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""


import socketserver
import threading
import sys
import time
from recv_rtp import RTPHandler

try:
    ServerSIP = sys.argv[1]
    IPPORT = ServerSIP.split(":")
    IPServerSIP = IPPORT[0]
    PortServerSIP = int(IPPORT[1])
    IP = '127.0.0.1'
    PORT = 6001

    Service = sys.argv[2]

except IndexError:
    print("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>")


class RTP1Handler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    Client_DIR = []

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        data = self.request[0]
        sock = self.request[1]
        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        print(TIME + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1]) + ":"
              + str(data.decode('utf-8')))

        recibido = data.decode('utf-8')
        metodo = recibido.split()[0]

        if metodo == "SIP/2.0":
            pass
        elif metodo == "INVITE":
            recibido1 = recibido.splitlines()[4:]
            pos3 = recibido1[3]
            pos3a = pos3.split("sip:")
            ipclient = pos3a[1].split(" ")[1]
            pos6 = recibido1[6].split("audio ")[1]
            portclient = pos6[0:5]
            self.Client_DIR.append(ipclient)
            self.Client_DIR.append(portclient)
            sock.sendto(f"SIP/2.0 200 OK\r\n\r\n".encode(), self.client_address)
            print(TIME + " SIP to aaaa" + self.client_address[0] + ":" + str(self.client_address[1])
                  + ":" + "SIP/2.0 200 OK\r\n\r\n")

        elif metodo == "ACK":
            print("RTP ready: " + str(self.Client_DIR[1]))
            RTPHandler.open_output("recibido.mp3")
            with socketserver.UDPServer(('127.0.0.1', 34543), RTPHandler) as serv:   # 64393
                print("Listening...")
                # El bucle de recepción (serv_forever) va en un hilo aparte,
                # para que se continue ejecutando este programa principal,
                # y podamos interrumpir ese bucle más adelante
                threading.Thread(target=serv.serve_forever).start()
                # Paramos un rato, Igualmente podríamos esperar a recibir BYE,
                # por ejemplos
                time.sleep(20)
                print("Time passed, shutting down receiver loop.")
                # Paramos el bucle de recepción, con lo que terminará el thread,
                # y dejamos de recibir paquetes
                serv.shutdown()
            # Cerramos el fichero donde estamos escribiendo los datos recibidos
            RTPHandler.close_output()
            print("RTP all content received " + str(self.client_address[1]))

        elif metodo == "BYE":
            OKfinal = "SIP/2.0 200 OK"
            sock.sendto(OKfinal.encode(), self.client_address)
            print(TIME + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1])
                  + ":" + OKfinal + "\r\n\r\n")


def main():
    # Listens at port (my address)
    # and calls the SIPRegisterHandlers class to manage the request

    Register = "REGISTER sip:" + Service + "@songs.net SIP/2.0\r\n\r\n"

    try:
        # my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        serv = socketserver.UDPServer(('0.0.0.0', 0), RTP1Handler)
        print(" Starting...")
        serv.socket.sendto(Register.encode('utf-8'), (IPServerSIP, PortServerSIP))
        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        print(TIME + " SIP to " + IPServerSIP + ":" + str(PortServerSIP) + ":" + Register)

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
