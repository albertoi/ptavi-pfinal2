#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""


import sys
import socket
import simplertp
import time


IPServerSIP = sys.argv[1].split(":")[0]
portServerSIP = int(sys.argv[1].split(":")[1])
AddrServerRTP = sys.argv[3]
AddrClient = sys.argv[2]
sip1 = AddrClient.split(":")
sip = sip1[0]
if sip != "sip":
    sys.exit(
        "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>")
elif len(sys.argv) != 5:
    print("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>")
    sys.exit()


def main():

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        print(TIME + " Starting client...")
        register = "REGISTER " + AddrClient + " SIP/2.0\r\n\r\n"
        # Enviamos el registrar al serverSIP
        my_socket.sendto(register.encode('utf-8'), (IPServerSIP, portServerSIP))
        r = my_socket.recv(1024).decode('utf-8')
        r = r.splitlines()
        r1 = r[0]
        if r1 == "SIP/2.0":
            pass
        print(TIME + " SIP to" + IPServerSIP + ":" + str(portServerSIP) + ":" + register)
        # Petición INVITE a servidor SIP
        peticion1 = "INVITE" + " " + AddrServerRTP + " SIP/2.0\r\n"
        From = "From:<" + AddrClient + ">"
        # Definimos los parámetros en formato SDP de nuestro INVITE
        c = "Content-Type: application/sdp"
        c1 = "Content-Length:"
        v = "v=0"
        o = "o=" + AddrClient + " 127.0.0.1"
        t = "t=0"
        m = "m=audio 34543 RTP"
        body0 = v + o + "s = cliente1" + t + m
        body = c + "\r\n" + c1 + " " + str(len(body0)) + "\r\n\r\n" + v + "\r\n" + o + "\r\n" + "s=cliente1" \
            + "\r\n" + t + "\r\n" + m

        # Primer invite que se realizará al server sip
        invite1 = peticion1 + From + "\r\n" + body
        print(invite1)
        # Enviamos el invite
        my_socket.sendto(invite1.encode('utf-8'), (IPServerSIP, portServerSIP))
        print(TIME + " SIP to " + IPServerSIP + ":" + str(portServerSIP) + ":" + AddrServerRTP)

        # Extraemos la respuesta, puerto, ip y contact en los siguientes pasos
        respuesta = my_socket.recv(1024).decode('utf-8')
        print(TIME + " SIP from " + IPServerSIP + ":" + str(portServerSIP) + ":" + respuesta)
        answer0 = respuesta.splitlines()
        contact = answer0[1].split(" ")[1]
        ServerRTP = contact.split(":")[1:]
        IPServerRTP = ServerRTP[0].split("@")[1]
        PortServerRTP = int(ServerRTP[1])
        # Se envía ACK al servidor SIP
        if answer0[0] == "SIP/2.0 302 Moved Temporarily":
            ack = "ACK " + AddrServerRTP + "SIP/2.0\r\n\r\n"
            print(TIME + " Sending ACK")
            my_socket.sendto(ack.encode('utf-8'), (IPServerSIP, portServerSIP))
            TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
            print(TIME + " SIP to " + IPServerSIP + ":" + str(portServerSIP) + ":" + ack)
            peticion2 = "INVITE" + " " + contact + " SIP/2.0\r\n"
            # Este será el invite segundo que queremos enviar al serverRTP
            invite2 = peticion2 + From + "\r\n\r\n" + body #
            print(invite2)
            # Enviamos el invite al serverRTP
            my_socket.sendto(invite2.encode('utf-8'), (IPServerRTP, PortServerRTP))
            print(TIME + " SIP to " + IPServerRTP + ":" + str(PortServerRTP) + ":" + peticion2)
        answer1 = my_socket.recv(1024).decode('utf-8')
        print(TIME + " SIP from " + IPServerRTP + ":" + str(PortServerRTP) + ":" + answer1)
        answer1 = answer1.splitlines()
        answer = answer1[0]
        # Ahora si recibimos el OK del RTP le enviamos ACK al serverRTP
        if answer == "SIP/2.0 200 OK":
            ack = "ACK " + contact + "SIP/2.0" + "\r\n" + " From:" + contact + "\r\n\r\n"
            print("Enviando ACK + Forms")
            my_socket.sendto(ack.encode('utf-8'), (IPServerRTP, PortServerRTP))
            print(TIME + " SIP to " + IPServerRTP + ":" + str(PortServerRTP) + ":" + ack)
            # Cuando enviamos ACK, acto seguido enviamos todos los paquetes RTP
            # a
            print(TIME + "RTP to " + IPServerRTP + ":" + str(PortServerRTP))
            # Creamos un objeto (sender) de la clase RTPSender, que se encargará de
            # enviar las muestras del fichero file a la dirección (ip, port) vía RT
            fichero = sys.argv[4]
            sender = simplertp.RTPSender(ip="127.0.0.1", port=34543, file=fichero, printout=True)
            # Para realizar el envío, llamamos a send_all_thread() de sender,
            # que realizará el envío de paquetes RTP en un hilo (thread) propio
            sender.send_threaded()
            # Como send_all_thread() ha creado su propio hilo, la ejecución sigue
            # inmediatamente por aquí, y nos podemos quedar esperando 2 segundos
            time.sleep(2)
            # Pasados los tres segundos, finalizamos el envío, tal y como esté en este
            # momento. El hilo (thread) terminará también.
            print("Finalizando el thread de envío.")
            sender.finish()
            # Enviamos BYE al server RTP para finalizar
            bye = "BYE " + contact + " SIP/2.0\r\n\r\n"
            my_socket.sendto(bye.encode('utf-8'), (IPServerRTP, PortServerRTP))
            print(TIME + " SIP to " + IPServerRTP + ":" + str(PortServerRTP) + ":" + bye)

            # OK del Servidor RTP, en respuesta al BYE
            # Ahora ya podemos terminar, lo haremos en el if
            answer2 = my_socket.recv(1024).decode('utf-8')
            print(TIME + " SIP from " + IPServerRTP + ":" + str(PortServerRTP) + ":" + answer2)
            answer2 = answer2.splitlines()
            answer2a = answer2[0]
            if answer2a == "SIP/2.0 200 OK":
                print(TIME + " Se ha recibido un:" + answer2a
                      + ", en respuesta al BYE, por lo tanto se termina la conexión.")
                my_socket.close()


if __name__ == "__main__":
    main()
