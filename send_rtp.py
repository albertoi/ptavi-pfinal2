#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Ejemplo de envío RTP
"""

import threading
import time

import simplertp

IP = '127.0.0.1'
PORT = 6010


def main():
    # Creamos un objeto (sender) de la clase RTPSender, que se encargará de
    # enviar las muestras del fichero file a la dirección (ip, port) vía RTP
    sender = simplertp.RTPSender(ip=IP, port=PORT, file='cancion.mp3', printout=True)
    # Para realizar el envío, llamamos a send_all_thread() de sender,
    # que realizará el envío de paquetes RTP en un hilo (thread) propio
    sender.send_threaded()
    # Commo send_all_thread() ha creado su propio hilo, la ejecución sigue
    # inmediatamente por aquí, y nos podemos quedar esperando 2 segundos
    time.sleep(2)
    # Pasados los tres segundos, finalizamos el envío, tal y como esté en este
    # momento. El hilo (thread) terminará también.
    print("Finalizando el thread de envío.")
    sender.finish()


if __name__ == "__main__":
    main()
